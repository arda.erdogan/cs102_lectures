package Exception_Handling.Example_1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class main {

    private static int[] array = {10, 45, 67, 3, 98};
    public static void main(String[] args) {
       first();
    }

    public static void first() {
        try {
           second();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Out of bounds.");
            first();
        }
    }

    public static void second() {
        try {
            third();
        } catch (InputMismatchException e) {
            System.out.println("Enter proper input, you idiot!");
            second();
        } catch (ArithmeticException e) {
            System.out.println("Oops, exception.");
            second();
        }
    }

    public static void third() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        int a = scanner.nextInt();

        System.out.print("Enter b: ");
        int b = scanner.nextInt();

        System.out.println(array[a/b]);
    }
}
