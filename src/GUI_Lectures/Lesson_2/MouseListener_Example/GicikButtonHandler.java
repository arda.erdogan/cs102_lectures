package GUI_Lectures.Lesson_2.MouseListener_Example;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

public class GicikButtonHandler extends MouseAdapter implements ActionListener {

    public void mouseEntered(MouseEvent event) {
        if (event.getSource() instanceof JButton) {
            JButton button = (JButton) event.getSource();

            Random rand = new Random();
            int x = rand.nextInt(350);
            int y = rand.nextInt(350);
            button.setLocation(x,y);
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JButton) {
            JOptionPane.showMessageDialog(null, "You Win!!!");
        }
    }
}
