package GUI_Lectures.Lesson_2.MouseClickCoordinates;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Simple Frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400,400);
        frame.setLayout(new FlowLayout());

        JLabel label = new JLabel("-,-");

        MyMouseListener listener = new MyMouseListener(label);
        frame.addMouseListener(listener);

        frame.add(label);
        frame.setVisible(true);
    }
}
