package GUI_Lectures.Lesson_2.EventExample_2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AnotherActionListener implements ActionListener {
    private int id;

    public AnotherActionListener(int id) {
        this.id = id;
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() instanceof JButton) {
            JButton button = (JButton) event.getSource();

            System.out.println("Listener: " + id + " from " + button.getText());
        }
    }
}
