package GUI_Lectures.Lesson_2.ChcekBoxExample;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Simple Frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400,400);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(6,1));
        frame.add(mainPanel);

        JCheckBox check1 = new JCheckBox("Check 1");
        JCheckBox check2 = new JCheckBox("Check 2");
        JCheckBox check3 = new JCheckBox("Check 3");
        JCheckBox check4 = new JCheckBox("Check 4");
        JCheckBox check5 = new JCheckBox("Check 5");

        ButtonGroup group1 = new ButtonGroup();
        group1.add(check1);
        group1.add(check2);
        group1.add(check3);
        group1.add(check4);
        group1.add(check5);

        mainPanel.add(check1);
        mainPanel.add(check2);
        mainPanel.add(check3);
        mainPanel.add(check4);
        mainPanel.add(check5);

        JButton button = new JButton("Report");
        mainPanel.add(button);

        button.addActionListener(new ButtonListener(check1,check2,check3,check4,check5));

        frame.setVisible(true);
    }
}
