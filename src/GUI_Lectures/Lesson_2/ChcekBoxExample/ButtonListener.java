package GUI_Lectures.Lesson_2.ChcekBoxExample;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListener implements ActionListener {
    private JCheckBox check1;
    private JCheckBox check2;
    private JCheckBox check3;
    private JCheckBox check4;
    private JCheckBox check5;

    public ButtonListener(JCheckBox check1, JCheckBox check2, JCheckBox check3, JCheckBox check4, JCheckBox check5) {
        this.check1 = check1;
        this.check2 = check2;
        this.check3 = check3;
        this.check4 = check4;
        this.check5 = check5;
    }

    public void actionPerformed(ActionEvent event) {
        String msg = "Status: [";
        msg += check1.isSelected();
        msg += ", " + check2.isSelected();
        msg += ", " + check3.isSelected();
        msg += ", " + check4.isSelected();
        msg += ", " + check5.isSelected() + "]";

        JOptionPane.showMessageDialog(null, msg);
    }

}
