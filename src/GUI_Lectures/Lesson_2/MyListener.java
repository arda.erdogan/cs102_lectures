package GUI_Lectures.Lesson_2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyListener implements ActionListener {

    public void actionPerformed(ActionEvent event) {
        String msg = "Button is clicked!";
        JOptionPane.showMessageDialog(null, msg);

    }
}
