package GUI_Lectures.Lesson_1;

import javax.swing.*;
import java.awt.*;

public class Lesson_BorderLayout {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400,400);

        JPanel mainPanel = new JPanel();
        frame.add(mainPanel);
        frame.add(new JButton("South"), BorderLayout.SOUTH);
        frame.add(new JButton("North"), BorderLayout.NORTH);

        mainPanel.setLayout(new BorderLayout());
        mainPanel.setBackground(Color.RED);

        mainPanel.add(new JButton("CENTER"), BorderLayout.CENTER);
        mainPanel.add(new JButton("South"), BorderLayout.SOUTH);
        mainPanel.add(new JButton("North"), BorderLayout.NORTH);
        mainPanel.add(new JButton("West"), BorderLayout.WEST);
        mainPanel.add(new JButton("East"), BorderLayout.EAST);

        frame.add(new JLabel("East"));
        frame.add(new JButton("Center"), BorderLayout.CENTER);

        frame.setVisible(true);
    }
}
