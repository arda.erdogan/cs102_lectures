package GUI_Lectures.Lesson_1;

import javax.swing.*;
import java.awt.*;

public class Lesson_1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Cs102_Lesson_1 App");

        int width = 400;
        int height = 400;
        frame.setSize(width,height);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        JPanel mainPanel = new JPanel();
        frame.add(mainPanel);
        mainPanel.setLayout(new FlowLayout());

        JButton button = new JButton("I am a Button");
        button.setBounds(200,200,150,150);
        mainPanel.add(button);


        Color color = new Color(13, 222, 56);
        button.setBackground(color);
        button.setForeground(new Color(128,0,0)); // blueish

        JButton button1 = new JButton("I am the second button");
        mainPanel.add(button1);
        button1.setBounds(10,10,200,60);

        button1.setBackground(new Color(25,89,90));
        button1.setForeground(color);

        JButton button2 = new JButton("ABC");
        mainPanel.add(button2);

        JTextField field = new JTextField("My size is manually set...");
        mainPanel.add(field);
//        field.setBounds(10,90,90,40);


        JTextArea area = new JTextArea("My size is manually set...");
        mainPanel.add(area);
        area.setBounds(10,90,90,40);

        frame.validate();
    }
}
