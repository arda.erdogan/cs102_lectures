package GUI_Lectures.Lesson_1;

import javax.swing.*;
import java.awt.*;

public class Lecture_GridLayout {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400,400);

        JPanel mainPanel = new JPanel();
        frame.add(mainPanel);
        mainPanel.setLayout(new GridLayout(5,5));

        mainPanel.add(new JLabel("One"));
        mainPanel.add(new JLabel("Two"));
        mainPanel.add(new JButton("Three"));
        mainPanel.add(new JTextField("Four"));
        mainPanel.add(new JButton("Five"));
        mainPanel.add(new JLabel("Six"));
        mainPanel.add(new JTextField("Seven"));
        mainPanel.add(new JButton("Eight"));
        mainPanel.add(new JLabel("ninehundredninetynine"));
        mainPanel.add(new JLabel("TEN"));

        frame.setVisible(true);

    }
}
