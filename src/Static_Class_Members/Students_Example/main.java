package Static_Class_Members.Students_Example;

public class main {
    public static void main(String[] args) {
        Student student = new Student("Arda");
        Student student2 = new Student("Ekin");
        System.out.println(Student.getStudentCount());

        Student student3 = new Student("Ece");

        System.out.println(Student.getStudentCount());
        System.out.println(student2.getId());
        System.out.println(student2.getStudentCount());

        FirstYearStudent firstYearStudent = new FirstYearStudent("Selin"); // 4
        Student firstYearStudent2 = new FirstYearStudent("Ezgi"); // 5

        System.out.println(Student.getStudentCount());
    }
}
