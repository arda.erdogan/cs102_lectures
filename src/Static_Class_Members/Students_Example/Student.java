package Static_Class_Members.Students_Example;

public class Student {
    private String name;
    private int id;
    private static int studentCount = 0;

    public Student(String name) {
        this.name = name;
        this.id = ++studentCount;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public static int getStudentCount() {
        return studentCount;
    }
}
